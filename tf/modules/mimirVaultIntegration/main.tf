terraform {
  required_providers {
    vault = {
      source = "hashicorp/vault"
      version = "3.15.2"
    }

    random = {
      source = "hashicorp/random"
      version = "3.5.1"
    }
  }
}

locals {
  db_root_user = "c3-vault"
}

resource "random_password" "db_initial_password" {
  length           = 24

  lower            = true
  upper            = true
  numeric          = true
  special          = true

  override_special = "!#$%&*()-_=+[]{}<>:?"

  keepers = {
    datacenter = "soc"
    region     = "se"
    version    = "1"
  }
}

# resource "vault_mount" "db" {
#   path = "c3tsdb"
#   type = "database"
# }

# resource "vault_database_secret_backend_connection" "connection" {
#   backend       = vault_mount.db.path
#   name          = "mimir"
#   plugin_name   = "mimir-database-plugin"
#   allowed_roles = []

#   mimir {
#     host = "mimir.soc.carboncollins.se"
#     port = "443"

#     tls = true

#     username = local.db_root_user
#     password = random_password.db_initial_password.result

#     username_template = "{{ printf 'dyn_%s_%s_%s' (.DisplayName | truncate 10) (.RoleName) (random 4) }}"
#   }
# }

resource "vault_kv_secret_v2" "example" {
  mount                      = "c3kv"
  name                       = "datacenter/soc/database/mimir"

  data_json                  = jsonencode({
    initialUsername = local.db_root_user,
    initialPassword = random_password.db_initial_password.result
  })

  custom_metadata {
    max_versions = 2
  }
}
