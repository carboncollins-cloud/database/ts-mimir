output "db_initial_password" {
  sensitive = true
  value = random_password.db_initial_password.result
}
