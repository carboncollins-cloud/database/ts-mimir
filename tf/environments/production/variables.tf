variable "mount_username" {
  type = string
  description = "The username used to authenticate"
  sensitive = true
}

variable "mount_password" {
  type = string
  description = "The password used to authenticate"
  sensitive = true
}
