job "database-ts" {
  name        = "Database Time Series (Mimir)"
  type        = "service"
  region      = "se"
  datacenters = ["soc"]
  namespace   = "c3-database"

  priority = 60

  group "mimir" {
    count = 1

    network {
      mode = "bridge"
    }

    restart {
      attempts = 10
      interval = "5m"
      delay    = "25s"
      mode     = "delay"
    }

    service {
      provider = "consul"
      name     = "mimir"
      port     = "9009"
      task     = "mimir"

      connect {
        sidecar_service {}
      }

      check {
        expose   = true
        name     = "Application Health Status"
        type     = "http"
        path     = "/health"
        interval = "10s"
        timeout  = "3s"
      }

      tags = [
        "internal-proxy.enable=true",
        "internal-proxy.consulcatalog.connect=true",
        "internal-proxy.http.routers.mimir.entrypoints=https",
        "internal-proxy.http.routers.mimir.rule=Host(`mimir.soc.carboncollins.se`)",
        "internal-proxy.http.routers.mimir.tls=true",
        "internal-proxy.http.routers.mimir.tls.certresolver=lets-encrypt",
        "internal-proxy.http.routers.mimir.tls.domains[0].main=*.soc.carboncollins.se"
      ]
    }

    task "mimir" {  
      driver = "docker"
      leader = true

      resources {
        cpu    = 2000
        memory = 3072
      }

      config {
        image = "grafana/mimir:2.8.0"

        args = ["--config.file=${NOMAD_TASK_DIR}/mimir-demo.yaml"]
      }

      env {
        TZ="Europe/Stockholm"
      }

      template {
        data = <<EOH
[[ fileContents "./config/mimir-demo.yaml.tpl" ]]
        EOH

        destination = "local/mimir-demo.yaml"
      }
    }
  }

  reschedule {
    delay          = "10s"
    delay_function = "exponential"
    max_delay      = "10m"
    unlimited      = true
  }

  update {
    health_check      = "checks"
    min_healthy_time  = "10s"
    healthy_deadline  = "10m"
    progress_deadline = "15m"
    auto_revert       = true
  }

  meta {
    gitSha      = "[[ .gitSha ]]"
    gitBranch   = "[[ .gitBranch ]]"
    pipelineId  = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId   = "[[ .projectId ]]"
    projectUrl  = "[[ .projectUrl ]]"
    statefull   = "true"
  }
}
